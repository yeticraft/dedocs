FROM python:3

ENV PYTHONUNBUFFERED=1

WORKDIR /app

COPY requirements.txt .

RUN pip install -r requirements.txt

WORKDIR /docs

# Prep with 
#   docker run --rm -v ${PWD}/docs:/docs -it dedocs:latest new .

ENTRYPOINT ["/usr/local/bin/mkdocs"]
CMD ["serve", "-a", "0.0.0.0:8000", "-f", "./mkdocs.yml"]
