# DEDocs

An MKDocs (pythonic) implementation in docker.  Intended for quick and simple local usage.

## Start

Create a new project like so:

    docker run --rm -v ${PWD}/docs:/docs -it dedocs:latest new .

This will create a new site in the `docs` folder within the current directory.

## Run

To start the site up, monitoring for live updates, on your local system:

    docker-compose up

This will run the server and display debuggig info to the terminal it was run from.
